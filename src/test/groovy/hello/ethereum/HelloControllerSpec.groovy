package hello.ethereum

import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.MediaType
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class HelloControllerSpec extends Specification {

    @Shared @AutoCleanup EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer)
    @Shared @AutoCleanup RxHttpClient client = embeddedServer.applicationContext.createBean(RxHttpClient, embeddedServer.getURL())

    @Shared address

    void "test index"() {
        given:
        HttpRequest request = HttpRequest.GET('/hello')
        String rsp  = client.toBlocking().retrieve(request)
        println rsp

        expect:
        rsp.contains("Running in")
    }

    void "create message"() {
        given:
        HttpRequest request = HttpRequest.POST('/hello','Hello world').contentType(MediaType.TEXT_PLAIN)
        address  = client.toBlocking().retrieve(request)
        println "contract deployed at $address"

        expect:
        address
    }

    void "retrieve the message"() {
        given:
        HttpRequest<String> request = HttpRequest.GET("/hello/$address").accept(MediaType.TEXT_PLAIN_TYPE)
        String message = client.toBlocking().retrieve(request)
        println "curent message $message"

        expect:
        message == "Hello world"
    }


    void "update the message"() {
        given:
        HttpRequest<String> request = HttpRequest.PUT("/hello/$address","Goodbye").contentType(MediaType.TEXT_PLAIN)
        String newAddress = client.toBlocking().retrieve(request)
        println "message update and $newAddress must to be $address"

        expect:
        address == newAddress
    }

    void "retrieve the updated message"() {
        given:
        HttpRequest<String> request = HttpRequest.GET("/hello/$address").accept(MediaType.TEXT_PLAIN_TYPE)
        String message = client.toBlocking().retrieve(request)
        println "curent message $message"

        expect:
        message == "Goodbye"
    }


}
