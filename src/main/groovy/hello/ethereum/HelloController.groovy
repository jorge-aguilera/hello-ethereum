package hello.ethereum

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.Put;

@Controller("/hello")
class HelloController {

    protected final EtherService etherService
    HelloController( EtherService etherService){
        this.etherService = etherService
    }

    @Get(value="/", produces = MediaType.TEXT_PLAIN)
    String index() {
        "Running in $etherService.server network"
    }

    @Post(consumes = MediaType.TEXT_PLAIN, produces = MediaType.TEXT_PLAIN)
    String createMessage(@Body String message) {
        String ret = etherService.createMessage(message)
        ret
    }

    @Get(value="/{address}", produces= MediaType.TEXT_PLAIN)
    String readMessage(String address) {
        etherService.getMessage(address)
    }

    @Put(value="/{address}", consumes = MediaType.TEXT_PLAIN, produces = MediaType.TEXT_PLAIN)
    String updateMessage(String address, @Body String message) {
        etherService.updateMessage(address, message)
    }

}