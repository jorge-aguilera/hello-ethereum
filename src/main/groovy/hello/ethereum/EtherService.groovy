package hello.ethereum

import org.web3j.protocol.core.methods.response.TransactionReceipt
import org.web3j.tx.Transfer
import org.web3j.utils.Convert

import javax.annotation.PostConstruct
import javax.inject.Singleton
import org.web3j.crypto.Credentials
import org.web3j.crypto.WalletUtils
import org.web3j.greeter.Greeter
import org.web3j.protocol.Web3j
import org.web3j.protocol.http.HttpService
import org.web3j.tx.gas.ContractGasProvider
import org.web3j.tx.gas.DefaultGasProvider
import io.micronaut.context.annotation.Value

@Singleton
class EtherService {

    private Web3j web3j

    private Credentials credentials

    private ContractGasProvider contractGasProvider = new DefaultGasProvider();

    @Value('${ethereum.server}')
    String server

    @Value('${ethereum.token}')
    String token

    @Value('${ethereum.password}')
    String password

    @Value('${ethereum.wallet}')
    String wallet

    @PostConstruct
    init(){
        web3j = Web3j.build(new HttpService(
                "$server/$token"))

        println "Connected to Ethereum client version: ${web3j.web3ClientVersion().send().web3ClientVersion}"

        credentials = WalletUtils.loadCredentials( password, wallet)
        println "Credentials loaded"
    }

    TransactionReceipt sendFundsTo( String address, BigDecimal amount, String unit ){
        TransactionReceipt transferReceipt = Transfer.sendFunds(
                web3j, credentials,
                address,
                amount, Convert.Unit.fromString(unit)
        ).send()
        transferReceipt
    }

    String createMessage(String message){
        Greeter contract = Greeter.deploy(web3j, credentials, contractGasProvider, message).send()
        println "Greeter contract deployed at address $contract.contractAddress"
        contract.contractAddress
    }

    String getMessage(String address){
        Greeter contract = Greeter.load(address, web3j, credentials, contractGasProvider)
        contract.greet().send()
    }

    String updateMessage(String address, String message){
        Greeter contract = Greeter.load(address, web3j, credentials, contractGasProvider)
        contract.newGreeting(message).send()
        contract.contractAddress
    }
}